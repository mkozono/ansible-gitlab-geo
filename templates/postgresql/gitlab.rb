roles ['postgres_role']

postgresql['listen_address'] = '*'

gitlab_rails['db_host'] = '/var/opt/gitlab/postgresql'
gitlab_rails['auto_migrate'] = false

{% if geo_secondary is defined %}
#gitlab_rails['db_password'] = '{{ sql_user_password }}'
#geo_secondary_role['enable'] = true
#geo_secondary['db_fdw'] = true
{% endif %}

{% if geo_primary is defined %}
geo_primary_role['enable'] = true
postgresql['max_replication_slots'] = 1
{% endif %}

postgresql['trust_auth_cidr_addresses'] = {{ trust_auth_cidr_addresses | to_json }}

{% if md5_auth_cidr_addresses is defined %}
postgresql['md5_auth_cidr_addresses'] = {{ md5_auth_cidr_addresses | to_json }}
{% endif %}

{% if geo_primary is defined and sql_user_password %}
postgresql['sql_user_password'] = '{{ sql_user_password }}'
{% endif %}
