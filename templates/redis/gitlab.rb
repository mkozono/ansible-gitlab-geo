# Disable components that will not be on the GitLab application server
roles ['redis_master_role']

gitlab_rails['enable'] = false

redis['bind'] = '0.0.0.0'
redis['port'] = 6379
